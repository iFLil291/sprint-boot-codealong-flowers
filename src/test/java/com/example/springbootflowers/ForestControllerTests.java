package com.example.springbootflowers;

import com.example.springbootflowers.forest_domain.ForestController;
import com.example.springbootflowers.forest_domain.ForestEntity;
import com.example.springbootflowers.forest_domain.ForestRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ForestControllerTests {

    @Autowired
    ForestRepository forestRepository;

    @Autowired
    ForestController forestController;

    ForestEntity argyll;
    ForestEntity galloway;

    @BeforeEach
    void setUp() {
        argyll = forestRepository.save(
                new ForestEntity("Argyll", "The steep, moss-covered sides of Puck's Glen create a feeling of magic and mystery...")
        );

        galloway = forestRepository.save(
                new ForestEntity("Galloway", "Taking a rest at Bruntis Loch, on one of our Kirroughtree trails")
        );
    }

    @Test
    void test_all_forests_success() {
        // Given
        List<ForestController.ForestDTO> all = forestController.all();

        // Then
        assertEquals(
                Stream.of(argyll, galloway)
                        .map(ForestController::toDTO)
                        .collect(Collectors.toList()),
                all);
    }
}
