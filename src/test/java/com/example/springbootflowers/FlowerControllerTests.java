package com.example.springbootflowers;

import com.example.springbootflowers.flower_domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class FlowerControllerTests {
    @Autowired
    FlowerRepository flowerRepository;

    @Autowired
    FlowerController flowerController;

    Flower stringOfHearts;
    Flower dragonScale;

    @BeforeEach
    void setUp() {
        stringOfHearts = flowerRepository.save(
                new FlowerImpl(
                        "String of hearts",
                        "Gray/Red"));

        dragonScale = flowerRepository.save(
                new FlowerImpl(
                        "Dragon Scale",
                        "Green"));
    }

    @Test
    void test_get_flowers_success() {
        // Given
        List<FlowerController.FlowerDTO> flowers = flowerController.all();

        List<FlowerController.FlowerDTO> expectedFlowers =
                Stream.of(stringOfHearts, dragonScale)
                        .map(FlowerController::toDTO)
                        .collect(Collectors.toList());

        // Then
        assertEquals(
                expectedFlowers,
                flowers);
    }

    @Test
    void test_get_flower_success() {
        FlowerController.FlowerDTO flower = flowerController.get(stringOfHearts.getId());

        assertEquals("String of hearts", flower.getName());
        assertEquals("Gray/Red", flower.getColor());
    }

    @Test
    void test_create_flower_success() {
        // Given
        FlowerController.FlowerDTO flower = flowerController.createFlower(new FlowerController.CreateFlowerDTO("Lily"));

        // Then
        assertEquals("Lily", flower.getName());
        assertEquals("Orange", flower.getColor());
    }

    @Test
    void test_update_flower_success() {
        // Given
        FlowerController.FlowerDTO flower = flowerController.updateFlower(stringOfHearts.getId(), new FlowerController.UpdateFlowerDTO("Begonia", "Blue"));

        // Then
        assertEquals("Begonia", flower.getName());
        assertEquals("Blue", flower.getColor());
    }

    @Test
    void test_delete_flower_success() {
        flowerController.deleteFlower(dragonScale.getId());

        List<FlowerController.FlowerDTO> flowers = flowerController.all();
        List<FlowerController.FlowerDTO> expectedFlowers =
                Stream.of(stringOfHearts).sorted()
                        .map(FlowerController::toDTO)
                        .collect(Collectors.toList());

        assertEquals(expectedFlowers, flowers);
    }
}
