package com.example.springbootflowers.flower_domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "color")
@Getter
@Setter
public class ColorConfiguration {
    String initialColor;
}
