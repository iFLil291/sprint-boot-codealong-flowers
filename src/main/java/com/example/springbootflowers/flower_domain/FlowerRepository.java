package com.example.springbootflowers.flower_domain;

import java.util.List;
import java.util.Optional;

public interface FlowerRepository {
    List<Flower> all();
    Flower save(Flower flower);
    Optional<Flower> findById(String id);
    void delete(String id);
}
