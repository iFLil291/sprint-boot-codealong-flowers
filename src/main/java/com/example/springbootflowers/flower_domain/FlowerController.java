package com.example.springbootflowers.flower_domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/flowers")
@AllArgsConstructor
public class FlowerController {
    FlowerService flowerService;

    @PreAuthorize("hasAnyAuthority('USER') or hasAnyRole('USER')")
    @GetMapping
    public List<FlowerDTO> all() {
        return flowerService.all().stream()
                .map(FlowerController::toDTO)
                .collect(Collectors.toList());
    }

    @PreAuthorize("hasAnyAuthority('USER') or hasAnyRole('USER')")
    @GetMapping("/{id}")
    public FlowerDTO get(@PathVariable("id") String id){
        return toDTO(flowerService.get(id));
    }

    @PreAuthorize("hasAnyAuthority('USER') or hasAnyRole('USER')")
    @PostMapping
    public FlowerDTO createFlower(@RequestBody CreateFlowerDTO createFlowerDTO) {
        return toDTO(flowerService.createFlower(createFlowerDTO.getName()));
    }

    @RolesAllowed("ADMIN")
    @PutMapping("/{id}")
    public FlowerDTO updateFlower(@PathVariable("id") String id, @RequestBody UpdateFlowerDTO updateFlowerDTO) {
        return toDTO(flowerService.updateFlower(id, updateFlowerDTO.getName(), updateFlowerDTO.getColor()));
    }

    @RolesAllowed("ADMIN")
    @DeleteMapping("/{id}")
    public void deleteFlower(@PathVariable("id") String id) {
        flowerService.deleteFlower(id);
    }

    public static FlowerDTO toDTO(Flower flower) {
        return new FlowerDTO(
                flower.getId(),
                flower.getName(),
                flower.getColor()
        );
    }

    @Value
    public static class FlowerDTO {
        String id;
        String name;
        String color;
    }

    @Value
    public static class CreateFlowerDTO {
        String name;

        @JsonCreator
        public CreateFlowerDTO(@JsonProperty("name") String name) {
            this.name = name;
        }
    }

    @Value
    public static class UpdateFlowerDTO {
        String name;
        String color;

        @JsonCreator
        public UpdateFlowerDTO(@JsonProperty("name") String name,
                               @JsonProperty("color") String color) {
            this.name = name;
            this.color = color;
        }
    }
}
