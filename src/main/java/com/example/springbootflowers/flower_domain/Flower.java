package com.example.springbootflowers.flower_domain;

public interface Flower {
    String getId();
    String getName();
    void setName(String name);
    String getColor();
    void setColor(String color);
}
