package com.example.springbootflowers.flower_domain;

import lombok.Data;

import java.util.UUID;

@Data
public class FlowerImpl implements Flower {
    String id;
    String name;
    String color;

    public FlowerImpl(String name, String color) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.color = color;
    }
}
