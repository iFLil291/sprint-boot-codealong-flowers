package com.example.springbootflowers.flower_domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class FlowerServiceImpl implements FlowerService{

    FlowerRepository flowerRepository;
    ColorConfiguration colorConfiguration;

    public List<Flower> all() {
        return flowerRepository.all();
    }

    @Override
    public Flower get(String id) {
        return flowerRepository.findById(id).orElse(null);
    }

    public Flower createFlower(String name) {
        Flower flower = new FlowerImpl(name, colorConfiguration.getInitialColor());
        return flowerRepository.save(flower);
    }

    @Override
    public Flower updateFlower(String id, String name, String color) {
        Flower flower = flowerRepository.findById(id).orElse(null);
        flower.setName(name);
        flower.setColor(color);
        return flowerRepository.save(flower);
    }

    @Override
    public void deleteFlower(String id) {
        flowerRepository.delete(id);
    }
}
