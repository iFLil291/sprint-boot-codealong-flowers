package com.example.springbootflowers.flower_domain;

import java.util.List;

public interface FlowerService {

    List<Flower> all();

    Flower get(String id);

    Flower createFlower(String name);

    Flower updateFlower(String id, String name, String color);

    void deleteFlower(String id);
}
