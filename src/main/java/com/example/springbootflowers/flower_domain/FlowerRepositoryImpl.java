package com.example.springbootflowers.flower_domain;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class FlowerRepositoryImpl implements FlowerRepository {
    Map<String, Flower> flowers = new HashMap<>();

    public List<Flower> all() {
        return new ArrayList<>(flowers.values());
    }

    public Flower save(Flower flower) {
        flowers.put(
                flower.getId(),
                flower);

        return flower;
    }

    @Override
    public Optional<Flower> findById(String id) {
        return Optional.of(flowers.get(id));
    }

    @Override
    public void delete(String id) {
        flowers.remove(id);
    }
}
