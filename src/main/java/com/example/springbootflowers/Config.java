package com.example.springbootflowers;

import com.example.springbootflowers.flower_domain.*;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
@AllArgsConstructor
public class Config {
    FlowerRepository flowerRepository;
    ColorConfiguration colorConfiguration;

    @Bean
    FlowerRepository flowerRepository() {
        return new FlowerRepositoryImpl();
    }

    @Bean
    FlowerService flowerService() {
        return new FlowerServiceImpl(flowerRepository, colorConfiguration);
    }
}
