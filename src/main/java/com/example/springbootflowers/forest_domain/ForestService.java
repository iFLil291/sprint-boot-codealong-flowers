package com.example.springbootflowers.forest_domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ForestService {

    ForestRepository forestRepository;

    public List<ForestEntity> all() {
        return forestRepository.all();
    }
}
