package com.example.springbootflowers.forest_domain;

import lombok.Data;

import java.util.UUID;

@Data
public class ForestEntity {
    String id;
    String name;
    String description;

    public ForestEntity(String name, String description) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
    }
}
