package com.example.springbootflowers.forest_domain;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ForestRepository {
    Map<String, ForestEntity> forests = new HashMap<>();

    public List<ForestEntity> all() {
        return new ArrayList<>(forests.values());
    }

    public ForestEntity save(ForestEntity forestEntity) {
        forests.put(
                forestEntity.getId(),
                forestEntity);

        return forestEntity;
    }
}
