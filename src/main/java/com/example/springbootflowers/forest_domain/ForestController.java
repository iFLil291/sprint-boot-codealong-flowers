package com.example.springbootflowers.forest_domain;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/forest")
@AllArgsConstructor
public class ForestController {

    ForestService forestService;

    @GetMapping
    public List<ForestDTO> all() {
        return forestService.all().stream()
                .map(ForestController::toDTO)
                .collect(Collectors.toList());
    }

    public static ForestDTO toDTO(ForestEntity forestEntity) {
        return new ForestDTO(
                forestEntity.getId(),
                forestEntity.getName(),
                forestEntity.getDescription()
        );
    }

    @Value
    public static class ForestDTO {
        String id;
        String name;
        String description;
    }
}
