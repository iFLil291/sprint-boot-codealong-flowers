package com.example.springbootflowers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootFlowersApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootFlowersApplication.class, args);
    }

}
