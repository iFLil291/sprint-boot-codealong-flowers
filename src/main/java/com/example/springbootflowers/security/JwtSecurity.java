package com.example.springbootflowers.security;

import com.nimbusds.jose.shaded.json.JSONArray;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
@Profile("jwt")
public class JwtSecurity extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();

        http
                .authorizeRequests()
                .antMatchers("api/v1/forests").permitAll()
                .anyRequest()
//                .hasAuthority("ADMIN")
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(
                        source -> new JwtAuthenticationToken(
                                source, Stream.concat(jwtGrantedAuthoritiesConverter.convert(source).stream(),
                                        ((JSONArray) source
                                                .getClaimAsMap("realm_access")
                                                .get("roles"))
                                                .stream()
                                                .map(o -> (String) o)
                                                .map(SimpleGrantedAuthority::new))
                                .collect(Collectors.toList())));
    }
}
